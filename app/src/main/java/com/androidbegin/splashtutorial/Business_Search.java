package com.androidbegin.splashtutorial;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.AdapterView;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Notifications;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Business_Search extends Activity implements  OnClickListener {

    Button search, near_me, appointment;
    AutoCompleteTextView auto;
    TextView cat2, cat3;
    ProgressDialog pDialog;
    JSONArray Categories = null;
    ArrayList<HashMap<String, String>> arraylist;
    Spinner category;

    private static final String Category_URL = "http://app.arrangly.com/category";
//    private static final String TAG_CATEGORY = "CATEGORY";
    private static final String TAG_NAME = "Name";
    private static final String TAG_ID = "ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business__search);
        category = (Spinner) findViewById(R.id.spinner);
        auto = (AutoCompleteTextView) findViewById(R.id.auto_text);
        near_me = (Button) findViewById(R.id.btn_near);
        cat3 = (TextView) findViewById(R.id.textView3);
        cat2 = (TextView) findViewById(R.id.textView4);
        search = (Button) findViewById(R.id.button4);
        appointment = (Button) findViewById(R.id.btn_appoint);
        search.setOnClickListener(this);
        near_me.setOnClickListener(this);
        appointment.setOnClickListener(this);
        cat3.setOnClickListener(this);
        cat2.setOnClickListener(this);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        Typeface type = Typeface.createFromAsset(getAssets(), "fonts/helevetica.ttf");
        cat3.setTypeface(type);
     //   List cst1 = new ArrayList();
        //  ListView list=(ListView)findViewById(R.id.list);
        new GetCategory().execute();
       //  cst1.add("Select Category");
        //cst1.add("Hair Dresser");
        //cst1.add("Technician");
        //cst1.add("Electrician");
        //cst1.add("Doctor");
        //cst1.add("Plumber");
        category.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l)
            {

                String c_pos=parent.getItemAtPosition(position).toString();
                if(position==0)
                {
                    Toast.makeText(getApplicationContext(),"You Selected:"+c_pos,Toast.LENGTH_LONG).show();
                }
                if(position==1){
                    Toast.makeText(getApplicationContext(),"You Selected:"+c_pos,Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

         //ArrayAdapter<String> list1 = new ArrayAdapter<String>(this,
           //    R.layout.list_item,R.id.text1, cst1);

       // category.setPrompt("Select Category");
        // list.setAdapter(adapter);
        //list.setAdapter(list1);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_business__search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    public void onClick(View v) {
        if (v.getId() == R.id.btn_near) {

            Intent intent = new Intent(getApplicationContext(), Search_fev.class);
            intent.putExtra("Map Call", "electrician");
            startActivity(intent);
        }


        if (v.getId() == R.id.button4) {

            Intent intent = new Intent(getApplicationContext(), Search_fev.class);
            intent.putExtra("Map Call", "dentist");
            startActivity(intent);
        }
        if (v.getId() == R.id.btn_appoint) {
            Intent intent = new Intent(getApplicationContext(), Real_time_request.class);
            startActivity(intent);
        }
        if (v.getId() == R.id.textView3) {

            //  cat3.setBackgroundDrawable(R.drawable.text_selector);
            // cat3.setBackground(Color.BLUE);
            Intent intent = new Intent(getApplicationContext(), Search_fev.class);
            intent.putExtra("Map Call", "doctor");
            startActivity(intent);
            // cat3.setBackgroundColor(R.drawable.pressed_color);
        }
        if (v.getId() == R.id.textView4) {
            Intent intent = new Intent(getApplicationContext(), Search_fev.class);
            intent.putExtra("Map Call", "hairdresser");
            startActivity(intent);
        } else
            Toast.makeText(getApplicationContext(), "Search Category", Toast.LENGTH_SHORT).show();
    }

    public void onNothingSelected(AdapterView arg0) {
        // TODO Auto-generated method stub
        Toast.makeText(getApplicationContext(), "Select Category", Toast.LENGTH_LONG).show();
    }

    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    class GetCategory extends AsyncTask<Void, Void, Void> {

        String termId0,name0, name1;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Business_Search.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();
            // Showing progress dialog


        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // Creating service handler class instance
            Category_Service sh = new Category_Service();

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(Category_URL, Category_Service.GET);

            Log.d("Response: ", "> " + jsonStr);
            String cat_name="";
            if (jsonStr != null) {
                try {

                    JSONObject jObject = new JSONObject(jsonStr);
                    JSONObject categoryObject = jObject.getJSONObject("Categories");
                    JSONObject obj0 = categoryObject.getJSONObject("1");
                     termId0 =obj0.getString("id");
                    name0 =obj0.getString("name");
                    JSONObject obj1 = categoryObject.getJSONObject("2");
                    String termId1= obj1.getString("id");
                    name1 =obj1.getString("name");

                    // check your log for json response
                    Log.d("Login attempt",name0);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }

            return null;

        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            pDialog.dismiss();
            List list=new ArrayList();
            list.add(name0);
            list.add(name1);
            ArrayAdapter<String> list1 = new ArrayAdapter<String>(getApplicationContext(),
                          R.layout.list_item,R.id.text1,list);
            category.setAdapter(list1);
            auto.setAdapter(list1);
            Toast.makeText(getApplicationContext(),"Name"+""+termId0+name0+"",Toast.LENGTH_LONG).show();
            // Dismiss the progress dialog

            /**
             * Updating parsed JSON data into ListView
             * */

      //  SimpleAdapter adapter=new SimpleAdapter(getApplicationContext(),arraylist,R.layout.list_item,new String[]{TAG_NAME},
        //        new int[]{R.id.text1});
          //  category.setAdapter(adapter);

        }

        }

    }
