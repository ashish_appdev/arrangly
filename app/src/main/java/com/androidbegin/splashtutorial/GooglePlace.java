package com.androidbegin.splashtutorial;

/**
 * Created by ashish on 5/5/2015.
 */
public class GooglePlace {
    private String name;
    private String category;
    private String rating;
    private String open;
    private String distence;

    public GooglePlace() {
        this.name = "";
        this.rating = "";
        this.open = "";
        this.distence="";
        this.setCategory("");
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDistence(String distence){this.distence=distence;}

    public String getName() {
        return name;
    }

    public String getDistence() {
        return distence;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getRating() {
        return rating;
    }

    public void setOpenNow(String open) {
        this.open = open;
    }

    public String getOpenNow() {
        return open;
    }
}
